VTK 6.0.0
BSD license

Compiled on win 7 with:
* optional Qt and views modules
* GNUStep's mingw with g++ 4.6.1
* cmake 2.8.11.2 with msys makefiles
* Qt 4.8.4 (mingw)

=== COMPILATION ===
export PATH="/C/Qt/4.8.4/bin:/C/Program Files (x86)/CMake 2.8/bin:$PATH"
7z x vtk-6.0.0.zip
cd VTK6.0.0/
cmake -D BUILD_EXAMPLES=1 -D BUILD_SHARED_LIBS=1 -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/home/xilexio/vtk -D VTK_Group_Qt=1 -D VTK_Group_Views=1 -G "MSYS Makefiles" .

=== USAGE NOTES ===
In order for rendering to work, it must be initialized. One of options is to put this into precompiled header:

#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)

note: for some reason VTK_MATERIALS_DIRS is a hardcoded directory path in vtkRenderingOpenGLConfigure.h. it's used to locate files in vtkXMLSharer::LocateFile (and maybe in other places), but wrong path here shouldn't introduce problems to normal usage.